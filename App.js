import * as React from 'react';
import MainStackNavigator from './nav/appnav';

export default function App() {
  return (
    <MainStackNavigator />
  );
}