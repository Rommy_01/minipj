import React, { Component } from 'react';
import {View, Text,Image,Linking, ActivityIndicator,TouchableOpacity} from 'react-native';


export default class GameScreen extends Component  {
    constructor(props) {
        super(props);


        

    }
    render(){
        console.log("loaded")
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' ,backgroundColor: this.props.route.params.backgroundcolor,borderWidth: 15,borderColor:this.props.route.params.backgroundcolor}}>
              <Image style={{ width: 290, height: 150 ,resizeMode: 'contain'}} source={{ uri: this.props.route.params.pic }} />
              <Text style={{ fontSize: 48}}></Text>
              <Text style={{ fontSize: 28,color: this.props.route.params.textcolor, textAlign:'center' }}>{this.props.route.params.name}</Text>
              <Text style={{ fontSize: 48}}></Text>
              <Text style={{ fontSize: 16,color: this.props.route.params.textcolor, textAlign:'center'}} >      {this.props.route.params.desc}</Text>
              <Text style={{ fontSize: 96}}></Text>

              <TouchableOpacity style={{borderWidth: 2,borderColor:this.props.route.params.textcolor,padding: 5 }} onPress={ ()=>{ Linking.openURL(this.props.route.params.links)}}>
              <Text style={{ fontSize: 20,color: this.props.route.params.textcolor }}>Download</Text>
              </TouchableOpacity> 


            </View>
          );
    }
  }