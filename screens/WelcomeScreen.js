import * as React from 'react';
import { View, Text,Image } from 'react-native';
export default function WelcomeScreen() {
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center',margin: 15 }}>
    	<Image style={{ width: 280, height: 280 ,resizeMode: 'contain'}} source={require('../assets/icon2.png') } />
      <Text style = {{fontSize: 32}}>Welcome to Rom's game collection!</Text>
      <Text style={{ fontSize: 24}}></Text>
      <Text>This app contain list of free PC games for you to play during the outbreak hand-pick by me </Text>     
    </View>
  );

}