import * as React from 'react';
import { Text, View,Image } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import HomeScreen from '../screens/HomeScreen';
import GameScreen from '../screens/GameScreen';
import { createStackNavigator } from '@react-navigation/stack';
import WelcomeScreen from '../screens/WelcomeScreen';



const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();

function tabnav() {
  return (
     
      <Tab.Navigator  /*tabBarOptions={{
        labelStyle: {
          fontSize: 24,
          margin: 0,
          padding: 0,
        },
        activeTintColor: 'tomato',
        inactiveTintColor: 'gray',
      }}*/ >
        <Tab.Screen options={{ title: 'Welcome!' } } name="WelcomeScreen" component={WelcomeScreen} />
        <Tab.Screen options={{ title: 'Search' } } name="Search" component={HomeScreen}/>        

      </Tab.Navigator>
  );
}

function MainStackNavigator(){
  return (
    <NavigationContainer>
    <Stack.Navigator>
      <Stack.Screen options={{ title: 'Rom"s game collection',tabBarVisible: false } } name="tabnav" component={tabnav} />
      <Stack.Screen options={{ title: 'back' } } name="Game" component={GameScreen} />
    </Stack.Navigator>
    </NavigationContainer>

  )
}

export default MainStackNavigator;


